#**Math and Simple File Servers**

Source for UDP TCP templates : https://www.cs.cmu.edu/afs/cs/academic/class/15213-f99/www/class26/<filename>

##**Math Server - UDP**
----------------------
###Execution:
> - **Compilation** : make
> - **Run Server** : ./Mserver <port>
> - **Run Client** : ./Mclient <host> <port>


##**File Server - TCP**
--------------------
###Execution:
> - **Compilation** : make
> - **Run Server** : ./Fserver <port>
> - **Run Client** : ./Fclient <host> <port>